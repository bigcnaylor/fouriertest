% Start in a clean environment
clear
clc

% Constants
inPath = "SampleData.csv";
fftOutPath = "SampleData.octave.fft";
ifftOutPath = "SampleData.octave.ifft";

% Load header and data from the input file
[sampleRate_Hz, labels, data] = LoadFFT_DataFile( inPath );

% Determine the size of the dataset, which much be a power of 2 in length
[len,width] = size( data );
len2 = 2^( nextpow2( len ) );
if( len2 > len )
	len2 = len2 / 2;
endif

% Compute the output frequencies
bins_Hz = sampleRate_Hz * data(1:len2,1) / data( len2, 1 );

% Perform the FFT on the actual data.
fftResults = fft( data( 1:len2, 2:width ) );

% Write the output file
outFile = fopen( fftOutPath, "w" );
status = fprintf( outFile, "%f\nBins [Hz]", sampleRate_Hz );
for i = 2:width
	status = fprintf( outFile, "\t%s", char(labels(1,i)) );
endfor
status = fputs( outFile, "\n" );
dlmwrite( outFile, [bins_Hz, fftResults], '\t' );
status = fclose( outFile );
clear i outFile status;

% Compute the output times
times_sec = [0:len2-1]' / sampleRate_Hz;

% Perform the inverse FFT on the computed coefficients
ifftResults = ifft( fftResults );

% Write the output file
outFile = fopen( ifftOutPath, "w" );
status = fprintf( outFile, "%f\nTime [sec]", sampleRate_Hz );
for i = 2:width
	status = fprintf( outFile, "\t%s", char(labels(1,i)) );
endfor
status = fputs( outFile, "\n" );
dlmwrite( outFile, [times_sec, ifftResults], '\t' );
status = fclose( outFile );
clear i outFile status;



