% Start with a clean environment
clear;
clc;

% Used to increment the plot figure
figNum = 0;

% Load data
[octRate_Hz,      octLabels,      octData]      = LoadFFT_DataFile( "SampleData.octave.ifft"   );
[cppRealRate_Hz,  cppRealLabels,  cppRealData]  = LoadFFT_DataFile( "SampleData.csv.real.ifft" );

% Define a function that is useful for plotting the loaded data
function [figNum] = PlotIFFT_Data( name, labels, data, figNum )

  % Create a new figure for each data set
  figure( ++figNum );
  
  % Plot the specified data
  [len,width] = size( data );
  plot( data( :, 1 ), data( :, width:-1:2 ) );
  
  % Format the plot
  legend( labels( 1, width:-1:2 ) );
  xlabel( labels( 1, 1 ) );
  ylabel( "Value" );
  title( name );
  grid on;
end

% Define a function that plots differences in data using Octave output as the baseline.
function [figNum] = PlotIFFT_Diff( name, labels, baseLine, data, figNum )
  [len,width] = size( data ); % All data must be the same size
  diffData = ( data( :, 2:width ) - baseLine( :, 2:width ) ) / max( baseLine( :, 2:width ) );
  figNum = PlotIFFT_Data( name, labels, [data(:, 1), diffData ], figNum );
end

% Plot all the loaded data
figNum = PlotIFFT_Data( "Octave", octLabels, octData, figNum );
figNum = PlotIFFT_Data( "Cpp (real)", cppRealLabels, cppRealData, figNum );

% Plot the differences between evaluated sets
figNum = PlotIFFT_Diff( "Diff on Cpp (real)", cppRealLabels, cppRealData, octData, figNum );