% This function can be used to load FFT data and results
function [rate_Hz, labels, data] = LoadFFT_DataFile( path )
	
	% open the file for reading
	file = fopen( path, "r" );
	
	% The first line is a number that is the rate at which data was sampled
	rate_Hz = str2num( fgetl(file) );
	
	% The second line specifies the names of the data columns
	labels = strsplit( fgetl(file), '\t' );
	
	% Everything else is tab-delimited data
	data = dlmread( file, '\t' );
	
	% Close the file when done
	fclose( file );
end