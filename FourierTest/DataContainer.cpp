//	Copyright 2014 Christian Naylor

#include ".\DataContainer.h"

#include <fstream>
#include <sstream>

using namespace FourierTest;

////////////////////////////////////////////////////////////////////////////////

////////////////////
//
//	Local Functions
//
////////////////////

namespace
{
	/**
	*	\brief	Convert a string to a complex number
	*
	*	\par	Description
	*			Take in the provided string and create a complex number. The complex
	*			number is in the format expected by MATLAB/Octabe.  This is different
	*			than the default stream representation of a complex number.
	*
	*	\param	str The string to convert.
	*
	*	\return	A complex number with the converted contents of the provided string.
	*/
	std::complex<double> StringToComplex( const std::string& str )
	{
		//	Empty strings may be zero
		if( str.empty() )
			return 0;

		//	Some notations will use 'j' to represent the imaginary unit, but 'i' is more common
		//	and is the character used by MATLAB/Octave.
		//
		if( str.back() == 'i' )
		{
			//	Find the sign separating the real and imaginary parts of the number
			std::size_t iFind = str.find_first_of( "+-", 1 );

			//	If there is a letter 'e' prior to the sign it means it is in scientific notation.  Look
			//	for the next one.
			if( Parsing::Strings::AreEqualI( str[iFind - 1], 'e' ) )
				iFind = str.find_first_of( "+-", iFind + 1 );

			//	If an imaginary part was found, parse both parts.
			if( iFind < str.size() )
			{
				return std::complex<double>(	Parsing::Strings::FromString<double>( str.substr( 0, iFind ) ),
												Parsing::Strings::FromString<double>( str.substr( iFind, str.size() - 1 ) ) );
			}

			//	Otherwise the number was not in a valid format
			throw std::invalid_argument( "The string \"" + str + "\" does not represent a valid copmlex number." );
		}
			
		//	If the number does not end in 'i' then we can assume it is a real number
		return Parsing::Strings::FromString<double>( str );
	}

	/**
	*	\brief	Convert a complex number to a string.
	*
	*	\par	Description
	*			Take in the provided complex number and create a string
	*			containing the complex number in a format compatible with
	*			MATLAB / Octave.  This is different than the default stream
	*			representation of a complex number.
	*
	*	\param	cmp	The complex number to convert.
	*
	*	\return	A string representing the number in a MATLAB-compatible format.
	*/
	std::string ComplexToString( const std::complex<double>& cmp )
	{
		//	Start with the real part of the number
		std::string result = Parsing::Strings::ToString( cmp.real() );

		//	For non-negative numbers add a sign between the parts.  Negative numbers will already include "-".
		if( cmp.imag() >= 0 )
			result += '+';

		//	Finally, add the imaginary part
		result += Parsing::Strings::ToString( cmp.imag() );
		result += 'i';

		return result;
	}
}

////////////////////////////////////////////////////////////////////////////////

	////////////////////
	//
	//	Construction
	//
	////////////////////

DataContainer::DataContainer() :
	mSampleRate_Hz( 1.0 ),
	mData( '\t' )
{
}

////////////////////////////////////////////////////////////////////////////////

DataContainer::DataContainer( const std::string& path ) :
	DataContainer()
{
	mPath = path;
	Load( mPath );
}

//////////////////////////////////////////////////////////////////////////////////////////

	////////////////////
	//
	//  Properties
	//
	////////////////////

std::size_t	DataContainer::GetNumFields() const
{
	return mData.GetNumFields();
}

//////////////////////////////////////////////////////////////////////////////////////////

std::size_t DataContainer::GetNumRecords() const
{
	return mData.GetNumRecords();
}

//////////////////////////////////////////////////////////////////////////////////////////

bool DataContainer::GetIsEmpty() const
{
	return mData.GetIsEmpty();
}

////////////////////////////////////////////////////////////////////////////////

	////////////////////
	//
	//	Contents
	//
	////////////////////

void DataContainer::AppendField( const std::vector<double>& data, const std::string& label )
{
	mData.AppendField( data, label );
}

//////////////////////////////////////////////////////////////////////////////////////////

void DataContainer::AppendField( const std::vector<std::complex<double>>& data, const std::string& label )
{
	mData.AppendField( data, label, &ComplexToString );
}

//////////////////////////////////////////////////////////////////////////////////////////

void DataContainer::Clear()
{
	mSampleRate_Hz = 1.0;
	mData.Clear();
}

//////////////////////////////////////////////////////////////////////////////////////////

const std::string& DataContainer::GetLabel( std::size_t index ) const
{
	return mData.GetLabel( index );
}

//////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> DataContainer::GetFieldReal( std::size_t index ) const
{
	return mData.GetField<std::vector<double>>( index );
}

//////////////////////////////////////////////////////////////////////////////////////////

std::vector<std::complex<double>> DataContainer::GetFieldComplex( std::size_t index ) const
{
	return mData.GetField<std::vector<std::complex<double>>>( index, &StringToComplex );
}

////////////////////////////////////////////////////////////////////////////////

	////////////////////
	//
	//	Loading
	//
	////////////////////

void DataContainer::Load()
{
	Load( mPath );
}

//////////////////////////////////////////////////////////////////////////////////////////

void DataContainer::Load( const std::string& path )
{
	std::ifstream file( path );

	if( !file.is_open() )
		throw std::invalid_argument( "Unalbe to open the file \"" + path = "\" for reading." );

	ReadStream( file );
}

//////////////////////////////////////////////////////////////////////////////////////////

void DataContainer::ReadStream( std::ifstream& rInput )
{
	//	The entire first line should consist only of the original sample rate
	std::string firstLine;
	if( std::getline( rInput, firstLine ) )
		mSampleRate_Hz = Parsing::Strings::FromString<double>( firstLine );
	else
		throw std::invalid_argument( "The stream did not start with a double representing the sample rate." );

	//	Then parse the delimeted data, including headers
	mData.ReadStream( rInput, true );
}

////////////////////////////////////////////////////////////////////////////////

	////////////////////
	//
	//	Saving
	//
	////////////////////

void DataContainer::Save() const
{
	SaveAs( mPath );
}

//////////////////////////////////////////////////////////////////////////////////////////

void DataContainer::SaveAs( const std::string& path ) const
{
	std::ofstream file( path );

	if( !file.is_open() )
		throw std::invalid_argument( "Unable to open the file \"" + path + "\"for writing." );

	WriteStream( file );
}

//////////////////////////////////////////////////////////////////////////////////////////

void DataContainer::WriteStream( std::ostream& rOutput ) const
{
	rOutput << mSampleRate_Hz << std::endl;
	mData.WriteStream( rOutput, true );
}