//	Copyright 2014 Christian Naylor	

#pragma once

#include <complex>
#include <string>
#include <vector>
#include "Parsing\CSV_Data.h"

namespace FourierTest
{
	/**
	*	\breif	Container to parse and abstract Fourier Series Unit Test data.
	*/
	class DataContainer
	{
	public:

		/**
		*	\brief	Construction
		*/
		//@{

			/**
			*	\brief	Default contructor.
			*
			*	\par	Description
			*			Create a new empty data container.
			*/
			DataContainer();

			/**
			*	\brief	Constructor.
			*
			*	\par	Description
			*			Create a new data container by loading the specified file.
			*
			*	\param	path The path to the file to load.
			*/
			DataContainer( const std::string& path );

		//@}

		/**
		*	\brief	Properties
		*/
		//@{

			/**
			*	\brief	Get the path to the file to load.
			*/
			const std::string& GetPath() const { return mPath; }

			/**
			*	\brief	Set the path to the file to save.
			*
			*	\param	path	The new file to save the file contents to.
			*/
			void SetPath( const std::string& path ) { mPath = path; }

			/**
			*	\brief	Get the rate at which the data in this container was collected.
			*/
			double GetSampleRate_Hz() const { return mSampleRate_Hz; }

			/**
			*	\brief	Set the rate at which the data in this container was collected.
			*/
			void SetSampleRate_Hz( double rate_Hz ) { mSampleRate_Hz = rate_Hz; }

			/**
			*	\brief	Get the number of fields (columns) of data stored in the container.
			*/
			std::size_t	GetNumFields() const;

			/**
			*	\brief	Get the number or records (rows) of data stored in the container.
			*/
			std::size_t GetNumRecords() const;

			/**
			*	\brief true if not data exists in the container.
			*/
			bool GetIsEmpty() const;

		//@}

		/**
		*	\brief	Contents
		*/
		//@{

			/**
			*	\brief	Place a new field of real data at the end of the container.
			*
			*	\param	data	The vector of data to be appended.
			*	\param	label	The name given to the column of data.
			*/
			void AppendField( const std::vector<double>& data, const std::string& label );

			/**
			*	\brief	Place a new field of complex data at the end of the container.
			*
			*	\param	data	The vector of data to be appended.
			*	\param	label	The name given to the column of data.
			*/
			void AppendField( const std::vector<std::complex<double>>& data, const std::string& label );

			/**
			*	\brief	Empty the container.
			*/
			void Clear();

			/**
			*	\brief	Get the label of the field at the specified index.
			*
			*	\param	index	The index of the field to retrieve.
			*
			*	\return	The label of the field at the specified index.
			*/
			const std::string& GetLabel( std::size_t index ) const;

			/**
			*	\brief	Get a copy of the field at the specified index as a vector of real numbers.
			*
			*	\param	index	the index of the field to retrieve.
			*
			*	\return	A new vector containing the real numbers parsed from the specified field.
			*/
			std::vector<double> GetFieldReal( std::size_t index ) const;

			/**
			*	\brief	Get a copy of the field at the specified index as a vector of complex numbers.
			*
			*	\param	index	The index of the field to retrieve.
			*
			*	\return A new vector containing the complex numbers parsed from the specified field.
			*/
			std::vector<std::complex<double>> GetFieldComplex( std::size_t index ) const;

		//@}

		/**
		*	\brief	Loading
		*/
		//@{

			/**
			*	\brief	Parse the data from the current path.
			*
			*	\par	Description
			*			Populates the container with data from the configured file.  Any
			*			existing data is removed from the stream first.
			*/
			void Load();

			/**
			*	\brief	Parse the data from the specified path.
			*
			*	\par	Description
			*			Populates the container with data from the specified file.  Any
			*			existing data is removed from the stream first.
			*
			*	\param	path	The path to the file to be loaded.
			*/
			void Load( const std::string& path );

			/**
			*	\brief	Parse the data from the provided stream.
			*
			*	\par	Description
			*			Populates the container with data from the provided stream.  Any
			*			existing data is removed from the container first.
			*
			*	\param	rInput		A reference to the stream that contains the data to be parsed.
			*/
			void ReadStream( std::ifstream& rInput );

		//@}

		/**
		*	\brief	Saving
		*/
		//@{

			/**
			*	\brief	Save the contents of the container back to the loaded file.
			*/
			void Save() const;

			/**
			*	\brief	Save the contents of the container to the specified file.
			*
			*	\param	path	The path to the destination file.
			*/
			void SaveAs( const std::string& path ) const;

			/**
			*	\brief	Place the contents of the container onto the provided stream.
			*
			*	\param	rOutput		The stream to place the data onto.
			*/
			void WriteStream( std::ostream& rOutput ) const;

		//@}

	private:

		/**
		*	\brief	Data
		*/
		//@{

			std::string	mPath;						/**< The path to the file to load.									*/
			double		mSampleRate_Hz;				/**< The rate at which the data in this container was collected.	*/

			Parsing::CSV_Data<std::string>	mData;	/**< The data stored in a comma-separated format.	*/

		//@}
	};
}