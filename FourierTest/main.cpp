//	Copyright 2014-2015 Christian Naylor

#include <cmath>
#include <iostream>
#include "Fourier\FFT.h"
#include "Fourier\Series.h"
#include "Math\Constants.h"
#include "Math\Math.h"
#include ".\DataContainer.h"

//////////////////////////////////////////////////////////////////////////////////////////

////////////////////
//
//  Local Functions
//
////////////////////

namespace
{
	/**
	*	\brief	Test the output of the fast Fourier Transform on a set of Real data.
	*
	*	\par	Description:
	*			Load a file containing columns of real numbers and perform a fast Fourier
	*			Transform on each.  The corresponding frequency bins and computed coefficients
	*			will be saved in a MATLAB-compatible output file for later evaluation.
	*
	*	\param	inFile	The path to the file to load.
	*/
	void TestRealForwardTransform( const std::string& inFile )
	{
		//	Load Data
		std::clog << "Beginning test of forward transform on real data..." << std::endl;
		std::clog << "\tLoading data from \"" << inFile << "\"." << std::endl;

		FourierTest::DataContainer input( inFile );
		if( input.GetIsEmpty() )
			return;

		//	Create a file of the ouput
		std::clog << "\tCreating output container." << std::endl;
		FourierTest::DataContainer output;

		//	Load the sample rate and the time column
		double sampleRate_Hz = input.GetSampleRate_Hz();
		output.SetSampleRate_Hz( sampleRate_Hz );

		//	Compute the normalized frequency bins.  The FFT algorithm only works on datasets that are
		//	some power of 2 in length, so trim the set to the largest possible power of 2
		std::clog << "\tComputing frequency bins." << std::endl;
		std::vector<double> bins;
		bins.reserve( input.GetNumRecords() );
		for( std::size_t iRow = 0, numRow = Math::LastPowerOfTwo( input.GetNumRecords() ), lastIndex = numRow - 1; iRow < numRow; ++iRow )
			bins.push_back( sampleRate_Hz * static_cast<double>( iRow ) / static_cast<double>( lastIndex ) );

		output.AppendField( bins, "Bins [Hz]" );

		//	Process each column of non-time (first column) data
		Fourier::Series<double> series( sampleRate_Hz );
		for( std::size_t iCol = 1, numCol = input.GetNumFields(); iCol < numCol; ++iCol )
		{
			auto& label = input.GetLabel( iCol );

			std::clog << "\tProcessing real data for \"" << label << "\"." << std::endl;
			series.Transform( input.GetFieldReal( iCol ) );

			std::clog << "\tAdding coefficients for \"" << label << "\" into output..." << std::endl;
			output.AppendField( series.GetCoefficients(), label );
		}

		//	Save the results
		auto outputPath = inFile + ".real.fft";
		std::clog << "\tSaving results to \"" << outputPath << "\".";
		output.SaveAs( outputPath );
	}

	/**
	*	\brief	Test the output of the fast Fourier Transform on a set of Complex data.
	*
	*	\par	Description:
	*			Load a file containing columns of complex numbers and perform a fast Fourier
	*			Transform on each.  The corresponding frequency bins and computed coefficients
	*			will be saved in a MATLAB-compatible output file for later evaluation.
	*
	*	\param	inFile	The path to the file to load.
	*/
	void TestComplexForwardTransform( const std::string& inFile )
	{
		//	Load Data
		std::clog << "Beginning test of forward transform on complex data..." << std::endl;
		std::clog << "\tLoading data from \"" << inFile << "\"." << std::endl;

		FourierTest::DataContainer input( inFile );
		if( input.GetIsEmpty() )
			return;

		//	Create a file of the ouput
		std::clog << "\tCreating output container." << std::endl;
		FourierTest::DataContainer output;

		//	Load the sample rate and the time column
		double sampleRate_Hz = input.GetSampleRate_Hz();
		output.SetSampleRate_Hz( sampleRate_Hz );

		//	Compute the normalized frequency bins.  The FFT algorithm only works on datasets that are
		//	some power of 2 in length, so trim the set to the largest possible power of 2
		std::clog << "\tComputing frequency bins." << std::endl;
		std::vector<double> bins;
		bins.reserve( input.GetNumRecords() );
		for( std::size_t iRow = 0, numRow = Math::LastPowerOfTwo( input.GetNumRecords() ), lastIndex = numRow - 1; iRow < numRow; ++iRow )
			bins.push_back( sampleRate_Hz * static_cast<double>( iRow ) / static_cast<double>( lastIndex ) );

		output.AppendField( bins, "Bins [Hz]" );

		//	Process each column of non-time (first column) data
		Fourier::Series<double> series( sampleRate_Hz );
		for( std::size_t iCol = 1, numCol = input.GetNumFields(); iCol < numCol; ++iCol )
		{
			auto& label = input.GetLabel( iCol );

			std::clog << "\tProcessing complex data for \"" << label << "\"." << std::endl;
			series.Transform( input.GetFieldComplex( iCol ) );

			std::clog << "\tAdding coefficients for \"" << label << "\" into output..." << std::endl;
			output.AppendField( series.GetCoefficients(), label );
		}

		//	Save the results
		auto outputPath = inFile + ".complex.fft";
		std::clog << "\tSaving results to \"" << outputPath << "\".";
		output.SaveAs( outputPath );
	}

	/**
	*	\brief	Test the output of the inverse fast Fourier Transform.
	*
	*	\par	Description:
	*			Load a file containing columns of complex coefficients and perform an inverse fast
	*			Fourier Transform on each.  The corresponding frequency bins and computed coefficients
	*			will be saved in a MATLAB-compatible output file for later evaluation.
	*
	*	\param	rootFile	The path to the file loaded to perform the real forward transform.
	*/
	void TestInverseTransform( const std::string& rootFile )
	{
		std::string inFile = rootFile + ".real.fft";

		//	Load Data
		std::clog << "Beginning test of inverse transform..." << std::endl;
		std::clog << "\tLoading data from \"" << inFile << "\"." << std::endl;

		FourierTest::DataContainer input( inFile );
		if( input.GetIsEmpty() )
			return;

		//	Create a file of the ouput
		std::clog << "\tCreating output container." << std::endl;
		FourierTest::DataContainer output;

		//	Load the sample rate and the time column
		double sampleRate_Hz = input.GetSampleRate_Hz();
		output.SetSampleRate_Hz( sampleRate_Hz );

		double timeStep_sec = sampleRate_Hz > 0		?
								1.0 / sampleRate_Hz	:
								0;

		//	Compute the time bines.  The FFT algorithm only works on datasets that are
		//	some power of 2 in length, so trim the set to the largest possible power of 2
		std::clog << "\tComputing time bins." << std::endl;
		std::vector<double> bins;
		bins.reserve( input.GetNumRecords() );
		for( std::size_t iRow = 0, numRow = Math::LastPowerOfTwo( input.GetNumRecords() ); iRow < numRow; ++iRow )
			bins.push_back( timeStep_sec * static_cast<double>( iRow ) );

		output.AppendField( bins, "Time [sec]" );

		//	Process each column of non-time (first column) data
		Fourier::Series<double> series( sampleRate_Hz );
		for( std::size_t iCol = 1, numCol = input.GetNumFields(); iCol < numCol; ++iCol )
		{
			auto& label = input.GetLabel( iCol );

			std::clog << "\tProcessing coefficients for \"" << label << "\"." << std::endl;
			series.Assign( input.GetFieldComplex( iCol ) );

			std::clog << "\tAdding data for \"" << label << "\" into output..." << std::endl;
			output.AppendField( series.Inverse(), label );
		}

		//	Save the results
		auto outputPath = rootFile + ".real.ifft";
		std::clog << "\tSaving results to \"" << outputPath << "\".";
		output.SaveAs( outputPath );
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

////////////////////
//
//  Main
//
////////////////////

int main( int argc, const char* argv[] )
{
	try
	{
		//	The first argument is the path to the executable, it will be ignored
		if( argc < 2 )
			throw std::invalid_argument( "Not enough command line arguments, nothing to process." );

		//	Open and process each file specified
		for( int iArg = 1; iArg < argc; ++iArg )
		{
			std::string timeDataPath = argv[iArg];

			//	Load the specified data as real numbers and then as complex numbers.  Perform a tranform on each.
			TestRealForwardTransform( timeDataPath );
			TestComplexForwardTransform( timeDataPath );

			//	Load the results of the real transform and perform an inverse transform
			TestInverseTransform( timeDataPath );
		}
	}
	catch( const std::exception& e )
	{
		std::clog << e.what();
	}
	catch( ... )
	{
		std::clog << "Unknown Exception.";
	}

	return 0;
}